package com.wxit.glxt.service;

import com.wxit.glxt.model.UserBean;

import java.util.List;

public interface UserService {
    public List<UserBean> findAll();
}
